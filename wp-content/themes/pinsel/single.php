<?php get_header(); ?>
<main class="pl-main">
	<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<section class="pl-section pl-object pl-content">
			<div class="pl-container">
					<h1><?php the_title(); ?></h1>
					<?php the_post_thumbnail('sculpture_main');?>
					<?php the_content(); ?>
			</div>		
		</section>			
	<?php endwhile; ?>
</main>
<?php get_footer(); ?>