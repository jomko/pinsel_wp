<?php get_header(); ?>
<main class="pl-main">
	<section class="pl-section pl-content">
		<div class="pl-container">
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
			<?php endwhile; ?>
		</div>		
	</section>
</main>
<?php get_footer(); ?>