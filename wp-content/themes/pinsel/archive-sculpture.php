<?php get_header(); ?>
<main>
	<section class="pl-gallery">
		<div class="pl-gallery__inner">
			<div class="swiper-container">
				<div class="swiper-wrapper">
					<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
						<div class="swiper-slide">
							<a class="pl-gallery__link" href="<?php the_permalink(); ?>">
								<div class="pl-gallery__slide">
									<div class="pl-gallery__image-container">
										<img src="<?php echo cmbf(get_the_ID(), '_img'); ?>" alt="<?php the_title(); ?>">
									</div>
									<p class="pl-gallery__title"><?php the_title(); ?></p>
								</div>
							</a>
						</div>						
					<?php endwhile; ?>
				</div>
				<div class="swiper-pagination"></div>
			</div>
		</div>		
	</section>
</main>
<?php get_footer(); ?>