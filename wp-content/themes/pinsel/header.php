<!DOCTYPE html>
<html lang="uk">
<head>
    <meta charset="utf-8">
    <title><?php echo wp_get_document_title(); ?></title>
    <meta name="description" content="Задум проекту полягає в тому, щоб осмислити спадщину Пінзеля за допомогою інструментів доповненої реальності.">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="<?php echo get_template_directory_uri(); ?>/img/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/img/og-image.jpg">
    <meta property="og:image:width" content="279">
    <meta property="og:image:height" content="279">
    <meta property="og:title" content="Pinsel AR">
    <meta property="og:url" content="pinsel-ar.com">
    <meta property="og:description" content="Задум проекту полягає в тому, щоб осмислити спадщину Пінзеля за допомогою інструментів доповненої реальності.">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,300i&amp;amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/css/swiper.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/css/iziModal.min.css">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <?php if (is_singular('sculpture')){ ?>
        <div class="sketchfab-bnt">
            <div class="sketchfab-bnt__inner">
                <a class="sketchfab-bnt__link" data-izimodal-open="#modal-frame">
                    <svg class="icon-fill icon-sketchfab ">
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/img/sprites/sprite-monocolor.svg#icon-sketchfab"></use>
                    </svg>
                </a>
            </div>
        </div>
        <div class="sketchfab-close">
            <div class="sketchfab-close__inner">
                <a class="sketchfab-bnt__link" data-izimodal-close="#modal-frame">
                    <svg class="icon-fill icon-close ">
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/img/sprites/sprite-monocolor.svg#icon-close"></use>
                    </svg>
                </a>
            </div>
        </div>
        <div class="modais" id="modal-frame" data-izimodal-transitionin="fadeIn" data-izimodal-iframeurl="<?php echo cmbf(get_the_ID(), '_link'); ?>"></div>
    <?php } ?>
    <!--Menu bgn-->
    <div class="burger-menu">
        <div class="burger-click-region"><span class="burger-menu-piece"></span><span class="burger-menu-piece"></span><span class="burger-menu-piece"></span></div>
    </div>
    <div class="pl-menu-block">
        <nav class="pl-navigation">
            <ul class="pl-navigation__list">
                <li class="pl-navigation__item"><a class="pl-navigation__link" href="/">Йоган-Георг Пінзель</a></li>
                <li class="pl-navigation__item"><a class="pl-navigation__link" href="/sculpture">Галерея</a></li>
                <!--li.pl-navigation__item-->
                <!--  a.pl-navigation__link(href="#") Місця Пінзеля-->
                <!--li.pl-navigation__item-->
                <!--  a.pl-navigation__link(href="#") Термінологія-->
                <li class="pl-navigation__item"><a class="pl-navigation__link" href="/bibliography">Бібліографія</a></li>
                <li class="pl-navigation__item"><a class="pl-navigation__link" href="/about">Про Pinsel.AR</a></li>
            </ul>
        </nav>
<!--         <div class="pl-languages"><span class="pl-languages__current">
        	<svg class="icon-fill icon-language ">
          	<use xlink:href="<?php //echo get_template_directory_uri(); ?>/img/sprites/sprite-monocolor.svg#icon-language"></use>
          </svg><span>Українська</span></span>
            <ul class="pl-languages-list">
                li.pl-languages__item
                  a.pl-languages__link(href="#") English
                li.pl-languages__item
                a.pl-languages__link(href="#") Français
                <li class="pl-languages__item"><a class="pl-languages__link" href="index_de.html">Deutsch</a></li>
                <li class="pl-languages__item"><a class="pl-languages__link" href="index_pl.html">Polski</a></li>
            </ul>
        </div> -->
        <ul class="pl-social">
            <!--li.pl-social__item-->
            <!--  a.pl-social__link(href="#")-->
            <!--    +icon-mono('icon-youtube')-->
            <li class="pl-social__item"><a class="pl-social__link" href="https://sketchfab.com/Mariia_K" target="_blank" rel="noopener"><svg class="icon-fill icon-sketchfab ">
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/img/sprites/sprite-monocolor.svg#icon-sketchfab"></use>
                    </svg></a></li>
            <!--li.pl-social__item-->
            <!--  a.pl-social__link(href="#")-->
            <!--    +icon-mono('icon-instagram')-->
            <li class="pl-social__item"><a class="pl-social__link" href="https://www.facebook.com/Pinsel.AR/" target="_blank" rel="noopener"><svg class="icon-fill icon-facebook ">
                        <use xlink:href="<?php echo get_template_directory_uri(); ?>/img/sprites/sprite-monocolor.svg#icon-facebook"></use>
                    </svg></a></li>
        </ul>
        <p class="pl-copy">© 2018. Pinsel AR</p>
    </div>
    <!--Menu end-->
    <!--Header bgn-->
    <header class="pl-header">
        <div class="pl-header__inner"><a class="pl-header__logo" href="/"><img src="<?php echo get_template_directory_uri(); ?>/img/pl-logo.svg" alt="Pinsel AR main logo" title="Pinsel AR"></a></div>
    </header>
    <!--Header end-->