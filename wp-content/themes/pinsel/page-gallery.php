<?php
/**
 * Template Name: Gallery
*/
get_header(); ?>
    <main>
        <section class="pl-gallery">
            <div class="pl-gallery__inner">
                <div class="swiper-container">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/avraam">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Avraam-back-640.png"></div>
                                    <p class="pl-gallery__title">Жертвоприношення Авраама</p>
                                </div>
                            </a></div>
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/ioan">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Ioan-back-640.png"></div>
                                    <p class="pl-gallery__title">Святий Іоан</p>
                                </div>
                            </a></div>
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/bohomatir">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Maria-back-640.png"></div>
                                    <p class="pl-gallery__title">Марія (Богоматір)</p>
                                </div>
                            </a></div>
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/putti-head-1">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Angel1-640.png"></div>
                                    <p class="pl-gallery__title">Крилата голівка обернена вправо</p>
                                </div>
                            </a></div>
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/putti-head-2">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Angel2-back-640.png"></div>
                                    <p class="pl-gallery__title">Крилата голівка обернена вправо</p>
                                </div>
                            </a></div>
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/putti-head-3">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Angel3-back-640.png"></div>
                                    <p class="pl-gallery__title">Крилата голівка з пасмом над чолом</p>
                                </div>
                            </a></div>
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/foot-1">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Foot-back.png"></div>
                                    <p class="pl-gallery__title">Частина скульптури “Святий Яким”</p>
                                </div>
                            </a></div>
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/foot-2">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Foot2-front-640.png"></div>
                                    <p class="pl-gallery__title">Частина скульптури “Святий Яким”</p>
                                </div>
                            </a></div>
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/aaron">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Aaron-back-640.png"></div>
                                    <p class="pl-gallery__title">Аарон</p>
                                </div>
                            </a></div>
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/angel-head">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Angel-head-640.png"></div>
                                    <p class="pl-gallery__title">Голова Ангела</p>
                                </div>
                            </a></div>
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/angel-putti">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Putti-back-640.png"></div>
                                    <p class="pl-gallery__title">Ангел Путто</p>
                                </div>
                            </a></div>
                        <div class="swiper-slide">
                        	<a class="pl-gallery__link" href="/felix">
                                <div class="pl-gallery__slide">
                                    <div class="pl-gallery__image-container"><img src="<?php echo get_template_directory_uri(); ?>/img/gallery/Felix-back-640.png"></div>
                                    <p class="pl-gallery__title">Святий Фелікс з Дитиною</p>
                                </div>
                            </a></div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </section>
    </main>
<?php get_footer(); ?>