<?php get_header(); ?>
<main class="pl-main">
	<section class="pl-section pl-content">
		<div class="pl-container">
			<h1>Error 404</h1>
			<h3>Page not found</h3>
		</div>		
	</section>
</main>
<?php get_footer(); ?>