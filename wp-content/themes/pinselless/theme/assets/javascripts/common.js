// Place your code here.
$(function() {

    // Loader hide
    setTimeout(function(){$('body').addClass('loaded');}, 100);

    // Load SVG-sprite on IE
    svg4everybody({});

    // Swiper init
    var mySwiper = new Swiper('.swiper-container', {
        slidesPerView: 3,
        speed: 1300,
        loop: true,
        centeredSlides: true,
        effect: 'coverflow',
        coverflowEffect: {
            rotate: 0,
            stretch: -60,
            depth: 150,
            modifier: 3,
            slideShadows : false,
        },
        // navigation: {
        //     prevEl: ".swiper-button-prev",
        //     nextEl: ".swiper-button-next"
        // },
        pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true
        },
        breakpoints: {
            // when window width is <= 480px
            480: {
                slidesPerView: 1,
            },
        }

    });


    // Burger and menu reveal
    var $mainMenuBlock = $('.pl-menu-block');
    var $burgerMenu  = $('.burger-click-region');
    var $pageCover = $('.page-cover').hide();
    var mainMenuTL = new TimelineMax();
    var clickDelay = 500;
    var clickDelayTimer = null;
    var mainMenu = {};

    mainMenuTL
        .set($mainMenuBlock, {left: '-270px'})
        .to($mainMenuBlock, 0.2, {left: 0, ease: Sine.easeOut});

    mainMenuTL.pause(0).invalidate();

    mainMenu.open = function () {
        mainMenuTL.play();
        $pageCover.fadeIn(200);
        $burgerMenu.addClass('active');
        $burgerMenu.parent().addClass('is-open');
        clickDelayTimer = setTimeout(function () {
            clearTimeout(clickDelayTimer);
            clickDelayTimer = null;
        }, clickDelay);
    };

    mainMenu.close = function () {
        mainMenuTL.reverse();
        $pageCover.fadeOut(200);
        $burgerMenu.removeClass('active');
        $burgerMenu.parent().removeClass('is-open');
        $burgerMenu.addClass('closing');
        clickDelayTimer = setTimeout(function () {
            $burgerMenu.removeClass('closing');
            clearTimeout(clickDelayTimer);
            clickDelayTimer = null;
        }, clickDelay);
    };

    $burgerMenu.click(function () {
        if(clickDelayTimer === null) {
            if ($burgerMenu.parent().hasClass('is-open')) {
                mainMenu.close();
            }
            else {
                mainMenu.open();
            }
        }
    });

    var $languageMenu = $('.pl-languages__list').hide();
    var $currentLanguage = $('.pl-languages_current');

    $currentLanguage.click(function () {
        if($(this).hasClass('is-open')) {
            $languageMenu.slideUp(200);
            $(this).removeClass('is-open')
        }
        else {
            $languageMenu.slideDown(200);
            $(this).addClass('is-open');
        }
    });

    $pageCover.click(function () {
        mainMenu.close();
    });

    if ($('.pl-sketchfab').length) {
        $(".modais").iziModal({
            history: false,
            iframe : true,
            fullscreen: false,
            headerColor: '#000000',
            width: '100%',
            iframeHeight: '99.9vh',
            top: 0,
            bottom: 0,
            borderBottom: false,
            radius: 0,
            closeButton: false,
            overlay: false,
            onOpened: function(){
                $('.sketchfab-close').addClass('show');
            },
            onClosing: function(){
                $('.sketchfab-close').removeClass('show');
            },
        });
        $('[data-izimodal-close]').click(function () {
            $('.modais').iziModal('close', {

            });
        });
    }

});